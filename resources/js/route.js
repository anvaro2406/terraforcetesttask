import {createRouter, createWebHistory} from 'vue-router';
import Login from "./pages/Login.vue";
import Register from "./pages/Register.vue";
import Home from "./pages/Home.vue";
import UpdateBook from "./pages/books/UpdateBook.vue";
import CreateBook from "./pages/books/CreateBook.vue";

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            component: Login,
        },
        {
            path: '/register',
            component: Register,
        },
        {
            path: '/home',
            name: 'home',
            component: Home,
        },
        {
            path: '/book/create',
            component: CreateBook,
        },
        {
            path: '/book/:id/edit',
            component: UpdateBook,
            props: true
        },
    ],
});

export default router;
