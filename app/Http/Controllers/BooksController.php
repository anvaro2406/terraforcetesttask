<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookRequest;
use App\Models\Book;
use App\Services\BookService;
use Illuminate\Http\JsonResponse;

class BooksController extends Controller
{
    /**
     * Display a listing of the books.
     */
    public function index(): JsonResponse
    {
        return response()->json([
            'books' => Book::all(),
        ]);
    }

    /**
     * Store a book in storage.
     */
    public function store(BookRequest $request): JsonResponse
    {
        $bookOrMessage = BookService::store($request->safe()->toArray());

        if (is_string($bookOrMessage)) {
            return response()->json([
                'message' => $bookOrMessage,
            ], 500);
        } else {
            return response()->json([
                'book' => $bookOrMessage,
            ]);
        }
    }

    /**
     * Display the specified book.
     */
    public function show(string $id): JsonResponse
    {
        $bookOrMessage = BookService::get($id);

        if (is_string($bookOrMessage)) {
            return response()->json([
                'message' => $bookOrMessage,
            ], 404);
        } else {
            return response()->json([
                'book' => $bookOrMessage,
            ]);
        }
    }

    /**
     * Update the book in storage.
     */
    public function update(BookRequest $request, string $id): JsonResponse
    {
        $bookOrMessage = BookService::update($request->safe()->toArray(), $id);

        if (is_string($bookOrMessage)) {
            return response()->json([
                'message' => $bookOrMessage,
            ], 500);
        } else {
            return response()->json([
                'book' => $bookOrMessage,
            ]);
        }
    }

    /**
     * Remove the book from storage.
     */
    public function destroy(string $id): JsonResponse
    {
        $resultMessage = BookService::delete($id);

        if (is_string($resultMessage)) {
            return response()->json([
                'message' => $resultMessage,
            ], 500);
        } else {
            return response()->json();
        }
    }
}
