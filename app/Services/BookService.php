<?php

namespace App\Services;

use App\Models\Book;
use Exception;
use Illuminate\Support\Facades\Log;
use Throwable;

class BookService
{
    /**
     * Get the book data.
     */
    public static function get(int $id)
    {
        try {
            return Book::findOrFail($id);
        } catch (Throwable $e) {
            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return 'Error: cannot find the book.';
        }
    }

    /**
     * Store a book in storage.
     */
    public static function store(array $fields)
    {
        try {
            return Book::create($fields);
        } catch (Throwable $e) {
            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return 'Error: cannot create the book.';
        }
    }

    /**
     * Update the book in storage.
     */
    public static function update(array $fields, string $id)
    {
        try {
            $book = Book::findOrFail($id);

            $book->fill($fields);

            $book->save();

            return $book;
        } catch (Throwable $e) {
            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return 'Error: cannot update the book.';
        }
    }

    /**
     * Remove the book from storage.
     */
    public static function delete(string $id): bool|string
    {
        try {
            if (!Book::destroy($id)) {
                throw new Exception("Model not found.", 404);
            }

            return true;
        } catch (Throwable $e) {
            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());

            return 'Error: cannot delete the book.';
        }
    }
}
